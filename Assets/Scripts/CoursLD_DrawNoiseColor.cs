﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoursLD_DrawNoiseColor : MonoBehaviour
{
    public void DrawNoiseColor(Renderer rend, int width, int height, Color[] color)
    {
        //creation de de la texture
        Texture2D textu = new Texture2D(width, height);

        textu.filterMode = FilterMode.Point;
        textu.wrapMode = TextureWrapMode.Clamp;

        textu.SetPixels(color);
        textu.Apply();

        //application sur le material
        rend.sharedMaterial.mainTexture = textu;
        rend.transform.localScale = new Vector3(width, 1, height);
    }
}
