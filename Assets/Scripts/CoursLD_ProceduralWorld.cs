using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoursLD_ProceduralWorld : MonoBehaviour
{
    [Tooltip("La largeur d'un chunk")]
    public int mapWidth;
    [Tooltip("La longeur d'un chunk")]
    public int mapHeight;

    public enum CombineMethod
    {
        None,
        Additive,
        Substractive,
        Multiply,
        Divide,
    }

    [System.Serializable]
    public struct NoiseParameter
    {
        [Tooltip("Nom du noise")]
        public string name;
        [Tooltip("L'échelle du noise")]
        public float noiseScale;
        [Tooltip("La seed du noise (permet de changer le profil du monde)")]
        public int seed;

        [Range(0, 1)]
        [Tooltip("Augmentation de la proportion de noir")]
        public float contrastMin;
        [Range(0, 1)]
        [Tooltip("Augmentation de la proportion de blanc")]
        public float contrastMax;
        [Range(0, 1)]
        [Tooltip("Attenuation du contraste (tend vers le gris)")]
        public float luminosityAtenuation;

        [Tooltip("Change l'origine du noise")]
        public Vector2 mapOrigin;

        [Tooltip("Le nombre d'octaves de déformation du noise (plus grandes est la valeur, plus les paramètres <persistances> et <lacunarity> auront d'effets, il s'agit en fait du nombre de noises superposés au premier)")]
        public int octaves;
        [Range(0, 1)]
        [Tooltip("A quel point les noises superposés auront un effet / A quel point l'amplitude des noises superposés est diminuée à chaque octaves")]
        public float persistance;
        [Tooltip("A quel point la fréquence de chaque noises superposés est augmenté en plus à chaque octaves")]
        public float lacunarity;

        [Tooltip("Couleur du noir")]
        public Color colorMin;
        [Tooltip("Couleur du blanc")]
        public Color colorMax;

        [HideInInspector]
        public float[,] noiseMap;
        private Color[] colorMap;

        [Tooltip("Le renderer du plane sur lequel se dessine le noise")]
        public Renderer textureRender;

        public Color[] GetColorMap()
        {
            return colorMap;
        }

        public void SetColorMap(Color[] color)
        {
            colorMap = color;
        }

        [Tooltip("Inverser le noir et le blanc")]
        public bool invert;
        [Tooltip("Méthode de fusion (toujours en additive pour le premier). Permet de fusionner ce noise aux autres ou non et de quelle manière.")]
        public CombineMethod fusionMethod;

        [HideInInspector]
        public Vector2 offset;
    }

    [Tooltip("Configurations des différents noises")]
    public NoiseParameter[] noiseParameters;

    [Tooltip("La hauteur maximal du block le plus haut")]
    public float heightMultiplier;
    [Tooltip("Profile du relief (N'accepte pas les valeurs négative. Au delà de 2 en absisce, n'a pas de sens.)")]
    public AnimationCurve curveHeightMultiplier;

    [System.Serializable]
    public struct BlocksLevel
    {
        [Tooltip("Nombre de block de sable au dessus de l'eau")]
        public int sandLevel;
        [Tooltip("Niveau de l'eau")]
        public int waterLevel;
        [HideInInspector]
        public int groundLevel;
        [Tooltip("Nombre de block de neige depuis le point le plus haut")]
        public int snowLevel;
        [Tooltip("Epaisseur de la couche de dirt")]
        public int dirtCount;
        [Tooltip("Epaisseur de la couche de sable")]
        public int sandCount;
        [Tooltip("Densité des arbres dans les biomes de forêts (agit comme un pourcentage pour chaque blocks dans la forêt: 0 = Aucun arbre, 100 = Un arbre sur chaque blocks)")]
        public float densityPercent;
    }

    [Tooltip("Parametrage des niveaux des blocks")]
    public BlocksLevel blockLevel;

    [System.Serializable]
    public struct Blocks
    {
        [Tooltip("Nom du block")]
        public string name;
        [Tooltip("Couleur du block (La couleur peut changer en fonction de la hauteur du block) (De la couleur au niveau le plus bas vers la couleur au niveau le plus haut)")]
        public Gradient blockColor;
    }

    [Tooltip("Tous les blocks pouvant être utilisés et leur couleur")]
    public Blocks[] blocks;

    [Tooltip("Permet de générer 9 chunk au lancement /!/ Génère beaucoup de blocks et réduit drastiquement les performances")]
    public bool generate9Chunk;

    [Tooltip("Permet de changer la visualisation du chunk, il s'agit surtout de Debug")]
    public int chunkCoordX = 0;
    [Tooltip("Permet de changer la visualisation du chunk, il s'agit surtout de Debug")]
    public int chunkCoordY = 0;

    [Tooltip("Mettre à jour automatiquement les noises et leur visualisation à chaque modification")]
    public bool autoUpdate;

    [HideInInspector]
    public float[,] noiseMapFusion;
    private Color[] colorMapFusion;

    [Tooltip("Renderer de la map retranscrivant le résultat final")]
    public Renderer fusionRend;

    private void Start()
    {
        blockLevel.sandLevel += blockLevel.waterLevel;

        CreateChunk(0, 0);

        if (generate9Chunk)
        {
            CreateChunk(1, 0);
            CreateChunk(-1, 0);
            CreateChunk(0, 1);
            CreateChunk(0, -1);
            CreateChunk(1, 1);
            CreateChunk(-1, 1);
            CreateChunk(-1, -1);
            CreateChunk(1, -1);
        }
    }

    void CreateChunk(int coordX,int coordY)
    {
        chunkCoordX = coordX;
        chunkCoordY = coordY;
        
        //On génère les noises
        Generate();

        //Génération du terrain
        CoursLD_GenerateFloor generateFloorScript = FindObjectOfType<CoursLD_GenerateFloor>();
        generateFloorScript.GenerateFloor(mapWidth, mapHeight, noiseMapFusion, curveHeightMultiplier, heightMultiplier, blockLevel, new Vector2(coordX, coordY),blocks);

        CoursLD_Combiner[] combiners = FindObjectsOfType<CoursLD_Combiner>();
        foreach (CoursLD_Combiner combiner in combiners)
            combiner.CombineMeshes();
    }

    
    public void Generate()
    {
        CoursLD_DrawNoiseColor drawNoiseColorScript = FindObjectOfType<CoursLD_DrawNoiseColor>();

        //calcule de tous les noises
        for (int i = 0; i < noiseParameters.Length; i++)
        {
            CoursLD_GenerateNoiseMap generateNoiseMapScript = FindObjectOfType<CoursLD_GenerateNoiseMap>();
            noiseParameters[i].noiseMap = generateNoiseMapScript.GenerateNoiseMap(mapWidth, mapHeight, noiseParameters[i], new Vector2(chunkCoordX,chunkCoordY));
            noiseParameters[i].SetColorMap(generateNoiseMapScript.GenerateColorMap(mapWidth, mapHeight, noiseParameters[i]));
            drawNoiseColorScript.DrawNoiseColor(noiseParameters[i].textureRender, mapWidth, mapHeight, noiseParameters[i].GetColorMap());
        }
        
        //Pour fusionner les scripts
        CoursLD_FusionneurDeNoise fusionScript = FindObjectOfType<CoursLD_FusionneurDeNoise>();
        noiseMapFusion = fusionScript.fusionNoise(noiseParameters, mapWidth, mapHeight);
        colorMapFusion = fusionScript.fusionColor(noiseParameters, mapWidth, mapHeight);
        
        //Afin de dessiner la couleur sur les planes
        drawNoiseColorScript.DrawNoiseColor(fusionRend, mapWidth, mapHeight, colorMapFusion);

        //On cherche le script du meshGenerator pour généré la vision en 3D de la heightMap
        CoursLD_MeshGenerator meshGeneratorScript = FindObjectOfType<CoursLD_MeshGenerator>();
        meshGeneratorScript.DrawMesh(meshGeneratorScript.GenerateTerrainMesh(noiseMapFusion, heightMultiplier, curveHeightMultiplier), meshGeneratorScript.TextureFromColorMap(colorMapFusion, mapWidth, mapHeight));
        
    }

    //Au moment de validé, pour évité les valeurs abérentes
    void OnValidate()
    {
        for (int i = 0; i < noiseParameters.Length; i++)
        {
            if (mapWidth < 1)
            {
                mapWidth = 1;
            }
            if (mapHeight < 1)
            {
                mapHeight = 1;
            }
            if (noiseParameters[i].lacunarity < 1)
            {
                noiseParameters[i].lacunarity = 1;
            }
            if (noiseParameters[i].octaves < 0)
            {
                noiseParameters[i].octaves = 0;
            }
            if (noiseParameters[i].contrastMax == noiseParameters[i].contrastMin)
            {
                if (noiseParameters[i].contrastMax > 0.5f)
                    noiseParameters[i].contrastMin -= 0.0001f;
                else
                    noiseParameters[i].contrastMax += 0.0001f;
            }
        }
    }
}
