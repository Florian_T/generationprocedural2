﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CoursLD_ProceduralWorld))]
public class CoursLD_MapGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        CoursLD_ProceduralWorld mapCoursLD = (CoursLD_ProceduralWorld)target;

        if (DrawDefaultInspector())
        {
            if (mapCoursLD.autoUpdate)
            {
                mapCoursLD.Generate();
            }
        }

        if (GUILayout.Button("Generate"))
        {
            mapCoursLD.Generate();
        }
    }

}
