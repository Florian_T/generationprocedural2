﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoursLD_GenerateNoiseMap : MonoBehaviour
{
    public float[,] GenerateNoiseMap(int width, int height, CoursLD_ProceduralWorld.NoiseParameter noiseParameter,Vector2 offset)
    {

        System.Random prng = new System.Random(noiseParameter.seed);
        Vector2[] octaveOffsets = new Vector2[noiseParameter.octaves];

        for (int i = 0; i < noiseParameter.octaves; i++)
        {
            float offsetX = prng.Next(-100000, 100000) + noiseParameter.mapOrigin.x;
            float offsetY = prng.Next(-100000, 100000) + noiseParameter.mapOrigin.y;
            octaveOffsets[i] = new Vector2(offsetX, offsetY);
        }

        if (noiseParameter.noiseScale <= 0)
        {
            noiseParameter.noiseScale = 0.0001f;
        }

        noiseParameter.noiseMap = new float[width, height];
        //noiseParameter.SetColorMap(new Color[width * height]);
        //Color[] color = new Color[width * height];
        float[,] noise = new float[width, height];

        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;

        float halfWidth = width / 2f;
        float halfHeight = height / 2f;

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                float amplitude = 1;
                float frequency = 1;
                float noiseHeight = 0;

                for (int i = 0; i < noiseParameter.octaves; i++)
                {
                    float sampleX = (offset.x*width + x - halfWidth) / noiseParameter.noiseScale * frequency + octaveOffsets[i].x;
                    float sampleY = (offset.y*height + y - halfHeight) / noiseParameter.noiseScale * frequency + octaveOffsets[i].y;

                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                    noiseHeight += perlinValue * amplitude;

                    noiseHeight *= noiseParameter.luminosityAtenuation;
                    
                    amplitude *= noiseParameter.persistance;
                    frequency *= noiseParameter.lacunarity;
                }

                //calcule du noise
                /*float xCoord = origin.x + x / (width * scale);
                float yCoord = origin.y + y / (height * scale);
                float sample = Mathf.PerlinNoise(xCoord, yCoord);

                sample = sample * luminosity;

                sample = Mathf.Clamp(sample, contrastMinimum, contrastMaximum);
                sample = Mathf.InverseLerp(contrastMinimum,contrastMaximum,sample);*/

                if (noiseHeight > maxNoiseHeight)
                {
                    maxNoiseHeight = noiseHeight;
                }
                else if (noiseHeight < minNoiseHeight)
                {
                    minNoiseHeight = noiseHeight;
                }

                noise[x, y] = noiseHeight;
            }
        }

        List<float> allNoisePoint = new List<float>();

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                noise[x, y] = Mathf.InverseLerp(-1, 1, noise[x, y]);
                allNoisePoint.Add(noise[x, y]);
                
                noise[x, y] = Mathf.Clamp(noise[x, y], noiseParameter.contrastMin, noiseParameter.contrastMax);
                noise[x, y] = Mathf.InverseLerp(noiseParameter.contrastMin, noiseParameter.contrastMax, noise[x, y]);

                if (noiseParameter.invert)
                {
                    noise[x, y] = noise[x,y] * -1 +1;
                }
            }
        }

        return noise;
    }

    public Color[] GenerateColorMap(int width,int height, CoursLD_ProceduralWorld.NoiseParameter noiseParameter)
    {
        Color[] color = new Color[width * height];

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                color[y * width + x] = Color.Lerp(noiseParameter.colorMin, noiseParameter.colorMax, noiseParameter.noiseMap[x, y]);
            }
        }

        return color;
    }
}
