﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoursLD_GenerateFloor : MonoBehaviour
{
    [Tooltip("Le block neutre, utilisé pour créer un block")]
    public GameObject patron;
    [Tooltip("GameObject permettant de récolter les blocks (ne sert plus, permettait de créer des meshs de plusieurs blocks")]
    public GameObject recolter;

    [System.Serializable]
    public struct ParentStructure
    {
        [Tooltip("Nom du parent")]
        public string name;
        [Tooltip("Le Transform du parent")]
        public Transform parent;
    }

    [Tooltip("Permet de ranger les blocks dans des parents différents lors de la génération")]
    public ParentStructure[] parents;

    [Tooltip("Asset de l'arbre présent dans les forêts")]
    public GameObject tree;

    

    public void GenerateFloor(int width, int height, float[,] noise, AnimationCurve curveHeightMultiplier, float heightMultiplier, CoursLD_ProceduralWorld.BlocksLevel blocksLevel, Vector2 offset, CoursLD_ProceduralWorld.Blocks[] blocks)
    {
        List<GameObject> blocksInTheChunk = new List<GameObject>();

        GameObject[] recolterEnCoursDUtilisation = new GameObject[blocks.Length];
        int[] nbOfVertexInTheRecolter = new int[blocks.Length];
        for (int i = 0; i < nbOfVertexInTheRecolter.Length; i++)
            nbOfVertexInTheRecolter[i] = 65536;

        //La hauteur maximale
        int floorHeightMax = (int)Mathf.Floor(curveHeightMultiplier.Evaluate(1) * heightMultiplier) + blocksLevel.groundLevel;
        //Création de l'array en 3D
        string[,,] _3DArrayString = new string[width, height, floorHeightMax + 1];
        
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                List<GameObject> blocksInTheColumn = new List<GameObject>();

                // Calcule de la hauteur du block en unité
                int floorHeight = (int)Mathf.Floor(curveHeightMultiplier.Evaluate(noise[x, y]) * heightMultiplier);

                // Surface Generation
                if (noise[x, y] >= 0)
                {
                    // Snow Surface Generation
                    if (floorHeight - blocksLevel.snowLevel > blocksLevel.snowLevel)
                    {
                        _3DArrayString[x, y, floorHeight] = "snow";
                    }
                    // Sand Surface Generation
                    else if (floorHeight < blocksLevel.sandLevel)
                    {
                        _3DArrayString[x, y, floorHeight] = "sand";

                    }
                    // Grass Surface Generation
                    else
                    {
                        Debug.Log(floorHeight);
                        _3DArrayString[x, y, floorHeight] = "grass";
                    }

                    // Trees Generation
                    if (FindObjectOfType<CoursLD_ProceduralWorld>().noiseParameters[3].noiseMap[x, y] >= 0.2f)
                    {
                        float random = Random.Range(0, 100);
                        if (random < blocksLevel.densityPercent && floorHeight > blocksLevel.sandLevel && floorHeight - blocksLevel.snowLevel< blocksLevel.snowLevel)
                        {
                            GameObject tree_clone = Instantiate(tree);
                            tree_clone.transform.parent = parents[6].parent;
                            tree_clone.transform.position = new Vector3(x + offset.x*width, floorHeight - 1, y+offset.y*height);
                            tree_clone.transform.Rotate(new Vector3(0, Random.Range(-180, 180), 0));
                        }
                    }
                }

                int dirtRandom = Random.Range(-1, 1);

                // Dirt Generation
                for (int i = floorHeight - 1; i >= blocksLevel.groundLevel + floorHeight - blocksLevel.dirtCount + dirtRandom; i--)
                {
                    if (Mathf.InverseLerp(0, floorHeight, (Mathf.Clamp(noise[x, y], noise[x, y], floorHeight))) <= floorHeight && i >= blocksLevel.groundLevel)
                    {
                        // Sand Generation
                        if (i <= blocksLevel.sandLevel)
                        {
                            _3DArrayString[x, y, i] = "sand";
                        }
                        else
                        {
                            _3DArrayString[x, y, i] = "dirt";
                        }
                    }
                }

                // Stone Generation
                for (int i = floorHeight - blocksLevel.dirtCount - 1 + dirtRandom; i >= blocksLevel.groundLevel; i--)
                {
                    if (Mathf.InverseLerp(0, floorHeight, (Mathf.Clamp(noise[x, y], noise[x, y], floorHeight))) <= floorHeight && i >= blocksLevel.groundLevel)
                    {
                        _3DArrayString[x, y, i] = "stone";
                    }
                }

                // Water Generation
                if (floorHeight <= blocksLevel.waterLevel)
                {
                    for (int i = floorHeight + 1; i < blocksLevel.waterLevel; i++)
                    {
                        _3DArrayString[x, y, i] = "water";
                    }
                }
            }
        }

        //Indicateurs de blocks suprimés
        int count =0;
        int countMax = 0;
        
        for (int x = 1; x < _3DArrayString.GetLength(0) - 1; x++)
        {
            for (int y = 1; y < _3DArrayString.GetLength(1) - 1; y++)
            {
                for (int z = 1; z < _3DArrayString.GetLength(2) - 1; z++)
                {
                    if (_3DArrayString[x, y, z] != null && _3DArrayString[x,y,z] != "water")
                    {
                        if ((_3DArrayString[x + 1, y, z] != null && _3DArrayString[x + 1, y, z] != "water") &&
                            (_3DArrayString[x, y + 1, z] != null && _3DArrayString[x, y + 1, z] != "water") &&
                            (_3DArrayString[x, y, z + 1] != null && _3DArrayString[x, y, z + 1] != "water") &&
                            (_3DArrayString[x - 1, y, z] != null && _3DArrayString[x - 1, y, z] != "water") &&
                            (_3DArrayString[x, y - 1, z] != null && _3DArrayString[x, y - 1, z] != "water") &&
                            (_3DArrayString[x, y, z - 1] != null && _3DArrayString[x, y, z - 1] != "water"))
                        {
                            _3DArrayString[x, y, z] = "PreAir";
                            count++;
                        }

                        countMax++;
                    }
                }
            }
        }

        /* 0 = grass
         * 1 = dirt
         * 2 = stone
         * 3 = sand
         * 4 = snow
         * 5 = water
         * 6 = trees
         */

        for (int x = 0; x < _3DArrayString.GetLength(0); x++)
        {
            for (int y = 0; y < _3DArrayString.GetLength(1); y++)
            {
                for (int z = 0; z < _3DArrayString.GetLength(2); z++)
                {
                    if (_3DArrayString[x, y, z] == "water")
                    {
                        CreateBlock(x, y, z, 5, "waterCube_Clone");
                    }
                    else if (_3DArrayString[x, y, z] == "snow")
                    {
                        CreateBlock(x, y, z, 4, "snowCube_Clone");
                    }
                    else if (_3DArrayString[x, y, z] == "sand")
                    {
                        CreateBlock(x, y, z, 3, "sandCube_Clone");
                    }
                    else if (_3DArrayString[x, y, z] == "stone")
                    {
                        CreateBlock(x, y, z, 2, "stoneCube_Clone");
                    }
                    else if (_3DArrayString[x, y, z] == "dirt")
                    {
                        CreateBlock(x, y, z, 1, "dirtCube_Clone");
                    }
                    else if (_3DArrayString[x, y, z] == "grass")
                    {
                        CreateBlock(x, y, z, 0, "grassCube_Clone");
                    }
                }
            }
        }

        void CreateBlock(int x, int y, int z, int blockId, string name)
        {
            GameObject cube = Instantiate(patron);
            cube.name = name;
            blocksInTheChunk.Add(cube);
            cube.transform.position = new Vector3(x + offset.x * width, z, y + offset.y * height);
            cube.GetComponent<Renderer>().material.color = blocks[blockId].blockColor.Evaluate((float)z / (float)floorHeightMax);
            cube.transform.parent = parents[blockId].parent;
            //StackInRecolter(blockId, cube);
        }

        void StackInRecolter(int blockId, GameObject block)
        {
            if (nbOfVertexInTheRecolter[blockId] <= 65536 - 8)
            {
                nbOfVertexInTheRecolter[blockId] += 8;
                block.transform.parent = recolterEnCoursDUtilisation[blockId].transform;
            }
            else
            {
                //Creation du recolter
                GameObject recolter_clone = Instantiate(recolter);
                recolter_clone.transform.parent = parents[blockId].parent;
                recolter_clone.GetComponent<Renderer>().material = block.GetComponent<Renderer>().material;
                recolter_clone.name = "Recolter_" + block.name;
                recolterEnCoursDUtilisation[blockId] = recolter_clone;

                //On ajoute le block dedans
                nbOfVertexInTheRecolter[blockId] = 8;
                block.transform.parent = recolterEnCoursDUtilisation[blockId].transform;
            }
        }

        Debug.Log("Blocks suprimés: " + count + " / " + countMax);
        Debug.Log("ChunkGenerated: " + offset);

        for (int i = 0; i < nbOfVertexInTheRecolter.Length; i++)
            Debug.Log("Nombre de vertex dans le slot: " + i + " = " + nbOfVertexInTheRecolter[i]);
    }
}
