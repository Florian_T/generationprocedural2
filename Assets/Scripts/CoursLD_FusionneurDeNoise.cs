﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoursLD_FusionneurDeNoise : MonoBehaviour
{
    public float[,] fusionNoise(CoursLD_ProceduralWorld.NoiseParameter[] noiseArray, int mapWidth, int mapHeight)
    {
        float[,] noiseMapFusion = new float[mapWidth, mapHeight];

        foreach (CoursLD_ProceduralWorld.NoiseParameter noise in noiseArray)
        {
            if (noise.fusionMethod != CoursLD_ProceduralWorld.CombineMethod.None)
            {
                for (int y = 0; y < mapHeight; y++)
                {
                    for (int x = 0; x < mapWidth; x++)
                    {
                        if (noise.fusionMethod == CoursLD_ProceduralWorld.CombineMethod.Additive)
                        {
                            noiseMapFusion[x, y] += noise.noiseMap[x, y];
                        }
                        if (noise.fusionMethod == CoursLD_ProceduralWorld.CombineMethod.Substractive)
                        {
                            noiseMapFusion[x, y] -= noise.noiseMap[x, y];
                        }
                        if (noise.fusionMethod == CoursLD_ProceduralWorld.CombineMethod.Multiply)
                        {
                            noiseMapFusion[x, y] *= noise.noiseMap[x, y];
                        }
                        if (noise.fusionMethod == CoursLD_ProceduralWorld.CombineMethod.Divide)
                        {
                            noiseMapFusion[x, y] *= 1 / (noise.noiseMap[x, y]);
                        }
                    }
                }
            }
        }

        return noiseMapFusion;
    }

    public Color[] fusionColor(CoursLD_ProceduralWorld.NoiseParameter[] noiseArray, int mapWidth, int mapHeight)
    {
        Color[] colorMapFusion = new Color[mapWidth * mapHeight];

        foreach (CoursLD_ProceduralWorld.NoiseParameter noise in noiseArray)
        {
            if (noise.fusionMethod != CoursLD_ProceduralWorld.CombineMethod.None)
            {
                for (int y = 0; y < mapHeight; y++)
                {
                    for (int x = 0; x < mapWidth; x++)
                    {
                        if (noise.fusionMethod == CoursLD_ProceduralWorld.CombineMethod.Additive)
                        {
                            colorMapFusion[y * mapWidth + x] += noise.GetColorMap()[y * mapWidth + x];
                        }
                        if (noise.fusionMethod == CoursLD_ProceduralWorld.CombineMethod.Substractive)
                        {
                            colorMapFusion[y * mapWidth + x] -= noise.GetColorMap()[y * mapWidth + x];
                        }
                        if (noise.fusionMethod == CoursLD_ProceduralWorld.CombineMethod.Multiply)
                        {
                            colorMapFusion[y * mapWidth + x] *= noise.GetColorMap()[y * mapWidth + x];
                        }
                        if (noise.fusionMethod == CoursLD_ProceduralWorld.CombineMethod.Divide)
                        {
                            colorMapFusion[y * mapWidth + x] *= noise.GetColorMap()[y * mapWidth + x];
                        }
                    }
                }
            }
        }

        return colorMapFusion;
    }
}
