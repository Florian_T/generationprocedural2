﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WorldGenerator : MonoBehaviour
{
    [SerializeField]
    private GameObject blockPrefab;

    [SerializeField]
    private int chunkSize = 50;

    [SerializeField]
    private float noiseScale = 0.05f;

    [SerializeField, Range(0, 1)]
    private float threshold = 0.5f;

    [SerializeField]
    private Material material;

    //Avoid memory problems
    private List<Mesh> meshes = new List<Mesh>();


    //Generation
    private void Generate()
    {
        //Performance indicator
        float startTime = Time.realtimeSinceStartup;

        #region  Create Mesh Data

        //Data for the final mesh
        List<CombineInstance> blockData = new List<CombineInstance>();
        //Create a unit cube and store the mesh from it
        MeshFilter blockMesh = Instantiate(blockPrefab, Vector3.zero, Quaternion.identity).GetComponent<MeshFilter>();

        //3D "For" for each position
        for(int x = 0; x < chunkSize; x++)
        {
            for(int y = 0; y < chunkSize; y++)
            {
                for(int z = 0; z < chunkSize; z++)
                {
                    //Get the value of the noise in the coordinates (x, y, z)
                    float noiseValue = Perlin3D(x * noiseScale, y * noiseScale, z * noiseScale);

                    //Place a block only if the value is above the threshold
                    if(noiseValue >= threshold)
                    {
                        //Move the generated unit block to the appropriate coordinates
                        blockMesh.transform.position = new Vector3(x, y, z);

                        //Copy the unit block data
                        CombineInstance ci = new CombineInstance
                        {
                            mesh = blockMesh.sharedMesh,
                            transform = blockMesh.transform.localToWorldMatrix
                        };

                        //Add it to the list
                        blockData.Add(ci);
                    }
                }
            }
        }

        //Data is already acquired, we destroy the object
        Destroy(blockMesh.gameObject);

        #endregion

        #region Separate Mesh Data

        //Meshes are limited to 65536 vertices, we divide them in lists of a max of 65536

        //Each sublist contains the data for one mesh
        List<List<CombineInstance>> blockDataLists = new List<List<CombineInstance>>();

        int vertexCount = 0;
        //Initial list of mesh data
        blockDataLists.Add(new List<CombineInstance>());
        for(int i = 0; i < blockData.Count; i++)
        {
            //Keep track of the total number of vertices
            vertexCount = vertexCount + blockData[i].mesh.vertexCount;

            //If we have too much vertices, reset the counter and store them in a new list
            if(vertexCount > 65536)
            {
                vertexCount = 0;
                blockDataLists.Add(new List<CombineInstance>());
                i = i - 1;
            }
            //Otherwise we can add another block to the list
            else
            {
                blockDataLists.Last().Add(blockData[i]);
            }

        }
        #endregion

        #region Create the final Mesh

        //Container object (parent)
        Transform container = new GameObject("MeshTotal").transform;
        foreach(List<CombineInstance> data in blockDataLists)
        {
            GameObject g = new GameObject("SmallMesh");
            g.transform.parent = container;
            MeshFilter mf = g.AddComponent<MeshFilter>();
            MeshRenderer mr = g.AddComponent<MeshRenderer>();
            mr.material = material;
            mf.mesh.CombineMeshes(data.ToArray());
            //Keep the ids of meshes we create to destroy them when it's no longer needed
            meshes.Add(mf.mesh);

            //Disabled for now (colliding), takes more time to render
            //g.AddComponent<MeshCollider>().sharedMesh = mf.sharedMesh;

        }


        #endregion

        //See the time for it to load
        Debug.Log("Generated in " + (Time.realtimeSinceStartup - startTime) + " seconds.");
    }


    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.G))
        {
            //Destroy parent
            Destroy(GameObject.Find("MeshTotal"));
            foreach(Mesh m in meshes)
            {
                Destroy(m);
            }

            Generate();
        }
    }
    
    public static float Perlin3D(float x, float y, float z)
    {

        

        float ab = Mathf.PerlinNoise(x, y);
        float bc = Mathf.PerlinNoise(y, z);
        float ac = Mathf.PerlinNoise(x, z);

        float ba = Mathf.PerlinNoise(y, x);
        float cb = Mathf.PerlinNoise(z, y);
        float ca = Mathf.PerlinNoise(z, x);

        float abc = ab + bc + ac + ba + cb + ca;
        return abc / 6f;
    }
    


}
