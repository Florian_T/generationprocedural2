﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;

/// <summary>
/// A simple free camera to be added to a Unity game object.
/// 
/// Keys:
///	wasd / arrows	- movement
///	q/e 			- up/down (local space)
///	r/f 			- up/down (world space)
///	pageup/pagedown	- up/down (world space)
///	hold shift		- enable fast movement mode
///	right mouse  	- enable free look
///	mouse			- free look / rotation
///     
/// </summary>
public class CoursLD_FreeCam : MonoBehaviour
{
    public enum TypeDeCamera
    {
        FreeCam,
        CameraTurn
    }

    public Transform target;

    public TypeDeCamera currentState;

    /// <summary>
    /// Normal speed of camera movement.
    /// </summary>
    public float movementSpeed = 10f;

    /// <summary>
    /// Speed of camera movement when shift is held down,
    /// </summary>
    public float fastMovementSpeed = 100f;

    /// <summary>
    /// Sensitivity for free look.
    /// </summary>
    public float freeLookSensitivity = 3f;

    /// <summary>
    /// Amount to zoom the camera when using the mouse wheel.
    /// </summary>
    public float zoomSensitivity = 10f;

    /// <summary>
    /// Amount to zoom the camera when using the mouse wheel (fast mode).
    /// </summary>
    public float fastZoomSensitivity = 50f;

    /// <summary>
    /// Set to true when free looking (on right mouse button).
    /// </summary>
    private bool looking = false;
    private Vector3 _lastPositionOfMouse;
    private Vector3 _currentPositionOfMouse;

    private void Start()
    {
        _lastPositionOfMouse = Input.mousePosition;
        _currentPositionOfMouse = Input.mousePosition;
    }

    void Update()
    {
        var fastMode = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
        var movementSpeed = fastMode ? this.fastMovementSpeed : this.movementSpeed;

        switch (currentState)
        {
            case TypeDeCamera.FreeCam:

                if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
                {
                    transform.position = transform.position + (-transform.right * movementSpeed * Time.deltaTime);
                }

                if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
                {
                    transform.position = transform.position + (transform.right * movementSpeed * Time.deltaTime);
                }

                if (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.UpArrow))
                {
                    transform.position = transform.position + (transform.forward * movementSpeed * Time.deltaTime);
                }

                if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
                {
                    transform.position = transform.position + (-transform.forward * movementSpeed * Time.deltaTime);
                }

                /*if (Input.GetKey(KeyCode.LeftShift))
                {
                    transform.position = transform.position + (transform.up * movementSpeed * Time.deltaTime);
                }

                if (Input.GetKey(KeyCode.LeftControl))
                {
                    transform.position = transform.position + (-transform.up * movementSpeed * Time.deltaTime);
                }*/



                if (looking)
                {
                    float newRotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * freeLookSensitivity;
                    float newRotationY = transform.localEulerAngles.x - Input.GetAxis("Mouse Y") * freeLookSensitivity;
                    transform.localEulerAngles = new Vector3(newRotationY, newRotationX, 0f);
                }

                if (Input.GetKeyDown(KeyCode.Mouse1))
                {
                    StartLooking();
                }
                else if (Input.GetKeyUp(KeyCode.Mouse1))
                {
                    StopLooking();
                }

                break;

            case TypeDeCamera.CameraTurn:

                transform.LookAt(target);

                _currentPositionOfMouse = Input.mousePosition;

                Vector3 delta;
                delta = Input.mousePosition - _lastPositionOfMouse;

                _lastPositionOfMouse = Input.mousePosition;

                if (Input.GetMouseButton(0))
                {
                    transform.RotateAround(target.position, Vector3.up, delta.x * 0.5f);
                    transform.RotateAround(target.position, transform.right, -delta.y * 0.5f);
                }

                break;
        }

        float axis = Input.GetAxis("Mouse ScrollWheel");
        if (axis != 0)
        {
            var zoomSensitivity = fastMode ? this.fastZoomSensitivity : this.zoomSensitivity;
            transform.position = transform.position + transform.forward * axis * zoomSensitivity;
        }

        if (Input.GetKey(KeyCode.R) || Input.GetKey(KeyCode.PageUp))
        {
            transform.position = transform.position + (Vector3.up * movementSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.F) || Input.GetKey(KeyCode.PageDown))
        {
            transform.position = transform.position + (-Vector3.up * movementSpeed * Time.deltaTime);
        }
    }

    void OnDisable()
    {
        StopLooking();
    }

    /// <summary>
    /// Enable free looking.
    /// </summary>
    public void StartLooking()
    {
        looking = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    /// <summary>
    /// Disable free looking.
    /// </summary>
    public void StopLooking()
    {
        looking = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void ChangeHowToLook(int type)
    {
        if (type == 0)
            currentState = TypeDeCamera.CameraTurn;
        if (type == 1)
            currentState = TypeDeCamera.FreeCam;
    }
}