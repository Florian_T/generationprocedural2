﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoursLD_ProceduralWorld_Old : MonoBehaviour
{
    public int mapWidth;
    public int mapHeight;

    public enum CombineMethod
    {
        Additive,
        Substractive,
        Multiply,
        Divide,
    }

    [System.Serializable]
    public struct NoiseParameter
    {
        public float noiseScale;
        public int seed;
        [Range(0, 1)]
        public float contrastMin;
        [Range(0, 1)]
        public float contrastMax;
        [Range(0, 1)]
        public float luminosityAtenuation;

        public Vector2 mapOrigin;

        //C'est pas encore tout fait ça
        public int octaves;
        [Range(0, 1)]
        public float persistance;
        public float lacunarity;
        //

        public Color colorMin;
        public Color colorMax;

        [HideInInspector]
        public float[,] noiseMap;
        [HideInInspector]
        private Color[] colorMap;

        public Renderer textureRender;

        public Color[] GetColorMap()
        {
            return colorMap;
        }

        public void SetColorMap(Color[] color)
        {
            colorMap = color;
        }

        public CombineMethod fusionMethod;
    }

    public NoiseParameter[] noiseParameters;

    public float heightMultiplier;
    public AnimationCurve curveHeightMultiplier;
    
    public bool autoUpdate;

    [HideInInspector]
    public float[,] noiseMapFusion;
    private Color[] colorMapFusion;

    public float icePosition = 0.9f;
    public float stonePosition = 0.7f;
    public float waterPosition = 0.3f;

    public Renderer fusionRend;
    public GameObject grassCube;
    public GameObject waterCube;
    public GameObject stoneCube;
    public GameObject iceCube;
    public GameObject sandCube;
   
    private void Start()
    {
        for (int i = 0; i < noiseParameters.Length; i++)
        {
            GenerateNoiseMap(mapWidth, mapHeight, noiseParameters[i].noiseScale, noiseParameters[i].contrastMin, noiseParameters[i].contrastMax, noiseParameters[i].luminosityAtenuation, noiseParameters[i].mapOrigin, noiseParameters[i].colorMin, noiseParameters[i].colorMax, noiseParameters[i].noiseMap, noiseParameters[i].GetColorMap(),i,noiseParameters[i].seed,noiseParameters[i].octaves, noiseParameters[i].lacunarity, noiseParameters[i].persistance);
            DrawNoiseMap(noiseParameters[i].textureRender, mapWidth, mapHeight, noiseParameters[i].GetColorMap());
        }

        fusion();

        GenerateFloor(mapWidth,mapHeight,noiseMapFusion);
    }


    public void Generate()
    {
        for (int i = 0; i < noiseParameters.Length; i++)
        {
            GenerateNoiseMap(mapWidth, mapHeight, noiseParameters[i].noiseScale, noiseParameters[i].contrastMin, noiseParameters[i].contrastMax, noiseParameters[i].luminosityAtenuation, noiseParameters[i].mapOrigin, noiseParameters[i].colorMin, noiseParameters[i].colorMax, noiseParameters[i].noiseMap, noiseParameters[i].GetColorMap(),i,noiseParameters[i].seed,noiseParameters[i].octaves,noiseParameters[i].lacunarity,noiseParameters[i].persistance);
            DrawNoiseMap(noiseParameters[i].textureRender, mapWidth, mapHeight, noiseParameters[i].GetColorMap());
        }

        fusion();
        MapDisplay display = FindObjectOfType<MapDisplay>();
        display.DrawMesh(MeshGenerator.GenerateTerrainMesh(noiseMapFusion,heightMultiplier, curveHeightMultiplier), TextureGenerator.TextureFromColorMap(colorMapFusion, mapWidth, mapHeight));
    }

    public void GenerateNoiseMap(int width, int height, float scale, float contrastMinimum, float contrastMaximum, float luminosity, Vector2 origin, Color colorMinimum, Color colorMaximum, float[,] noise, Color[] color, int ID,int seed,int octaves,float lacunarity,float persistance)
    {
        
        System.Random prng = new System.Random(seed);
        Vector2[] octaveOffsets = new Vector2[octaves];

        for (int i = 0; i < octaves; i++)
        {
            float offsetX = prng.Next(-100000, 100000) + noiseParameters[ID].mapOrigin.x;
            float offsetY = prng.Next(-100000, 100000) + noiseParameters[ID].mapOrigin.y;
            octaveOffsets[i] = new Vector2(offsetX, offsetY);
        }

        if (scale <= 0)
        {
            scale = 0.0001f;
            noiseParameters[ID].noiseScale = scale;
        }

        noise = new float[width, height];
        noiseParameters[ID].noiseMap = noise;
        color = new Color[width * height];
        noiseParameters[ID].SetColorMap(color);

        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;

        float halfWidth = width / 2f;
        float halfHeight = height / 2f;

        for (int y = 0; y < height; y++)
        {
            for(int x = 0; x < width; x++)
            {
                float amplitude = 1;
                float frequency = 1;
                float noiseHeight = 0;

                for (int i = 0; i < octaves; i++)
                {
                    float sampleX = (x - halfWidth) / scale * frequency + octaveOffsets[i].x;
                    float sampleY = (y - halfHeight) / scale * frequency + octaveOffsets[i].y;

                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                    noiseHeight += perlinValue * amplitude;
                    noiseHeight *= luminosity;

                    amplitude *= persistance;
                    frequency *= lacunarity;
                }

                //calcule du noise
                /*float xCoord = origin.x + x / (width * scale);
                float yCoord = origin.y + y / (height * scale);
                float sample = Mathf.PerlinNoise(xCoord, yCoord);

                sample = sample * luminosity;

                sample = Mathf.Clamp(sample, contrastMinimum, contrastMaximum);
                sample = Mathf.InverseLerp(contrastMinimum,contrastMaximum,sample);*/

                if (noiseHeight > maxNoiseHeight)
                {
                    maxNoiseHeight = noiseHeight;
                }
                else if (noiseHeight < minNoiseHeight)
                {
                    minNoiseHeight = noiseHeight;
                }

                noise[x, y] = noiseHeight;
            }
        }

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                noise[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noise[x, y]);
                noise[x, y] = Mathf.Clamp(noise[x, y], contrastMinimum, contrastMaximum);
                noise[x, y] = Mathf.InverseLerp(contrastMinimum, contrastMaximum, noise[x, y]);

                color[y * width + x] = Color.Lerp(colorMinimum, colorMaximum, noise[x,y]);

                noiseParameters[ID].noiseMap[x, y] = noise[x, y];
                noiseParameters[ID].SetColorMap(color);
            }
        }
    }

    public void DrawNoiseMap(Renderer rend, int width, int height, Color[] color)
    {
        //creation de de la texture
        Texture2D textu = new Texture2D(width, height);

        textu.filterMode = FilterMode.Point;
        textu.wrapMode = TextureWrapMode.Clamp;

        textu.SetPixels(color);
        textu.Apply();

        //application sur le material
        rend.sharedMaterial.mainTexture = textu;
        rend.transform.localScale = new Vector3(width, 1, height);
    }

    public void GenerateFloor(int width, int height, float[,] noise)
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                
                
                    GameObject grassCube_clone = Instantiate(grassCube);
                    grassCube_clone.transform.position = new Vector3(x, Mathf.Floor(curveHeightMultiplier.Evaluate(noise[x, y]) * heightMultiplier), y);
                    grassCube_clone.GetComponent<Renderer>().material.color = grassCube_clone.GetComponent<Renderer>().material.color + Color.green * noise[x, y];
                

                for(int z = 0; noise[x,y] <= waterPosition && z<= Mathf.Floor(curveHeightMultiplier.Evaluate(waterPosition) * heightMultiplier); z++  )
                {
                    GameObject waterCube_clone = Instantiate(waterCube);
                    waterCube_clone.transform.position = new Vector3(x, z, y);
                }

                /*if(noise[x,y] > stonePosition )
                {
                    GameObject stoneCube_clone = Instantiate(stoneCube);
                    stoneCube_clone.transform.position = new Vector3(x, Mathf.Floor(curveHeightMultiplier.Evaluate(noise[x, y]) * heightMultiplier), y);
                }*/

                /*if(noise[x,y] >= icePosition)
                {
                    GameObject iceCube_clone = Instantiate(stoneCube);
                    iceCube_clone.transform.position = new Vector3(x, Mathf.Floor(curveHeightMultiplier.Evaluate(noise[x, y]) * heightMultiplier), y);
                }*/
                
            }
        }
    }

    public void fusion()
    {
        noiseMapFusion = new float[mapWidth, mapHeight];
        colorMapFusion = new Color[mapWidth * mapHeight];

        foreach (NoiseParameter noise in noiseParameters)
        {
            for(int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    if (noise.fusionMethod == CombineMethod.Additive)
                    {
                        noiseMapFusion[x, y] += noise.noiseMap[x, y];
                        colorMapFusion[y * mapWidth + x] += noise.GetColorMap()[y * mapWidth + x];
                    }
                    if(noise.fusionMethod == CombineMethod.Substractive)
                    {
                        noiseMapFusion[x, y] -= noise.noiseMap[x, y];
                        colorMapFusion[y * mapWidth + x] -= noise.GetColorMap()[y * mapWidth + x];
                    }
                    if (noise.fusionMethod == CombineMethod.Multiply)
                    {
                        noiseMapFusion[x, y] *= noise.noiseMap[x, y];
                        colorMapFusion[y * mapWidth + x] *= noise.GetColorMap()[y * mapWidth + x];
                    }
                    if (noise.fusionMethod == CombineMethod.Divide)
                    {
                        noiseMapFusion[x, y] *= 1/(noise.noiseMap[x, y]);
                        colorMapFusion[y * mapWidth + x] *= noise.GetColorMap()[y * mapWidth + x];
                    }
                }
            }
        }

        DrawNoiseMap(fusionRend, mapWidth, mapHeight, colorMapFusion);
    }

    void OnValidate()
    {
        for (int i = 0; i < noiseParameters.Length; i++)
        {
            if (mapWidth < 1)
            {
                mapWidth = 1;
            }
            if (mapHeight < 1)
            {
                mapHeight = 1;
            }
            if (noiseParameters[i].lacunarity < 1)
            {
                noiseParameters[i].lacunarity = 1;
            }
            if (noiseParameters[i].octaves < 0)
            {
                noiseParameters[i].octaves = 0;
            }
            if (noiseParameters[i].contrastMax == noiseParameters[i].contrastMin)
            {
                if (noiseParameters[i].contrastMax > 0.5f)
                    noiseParameters[i].contrastMin -= 0.0001f;
                else
                    noiseParameters[i].contrastMax += 0.0001f;
            }
        }
    }
}
