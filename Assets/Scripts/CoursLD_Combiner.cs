﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class CoursLD_Combiner : MonoBehaviour
{
    public List<MeshFilter> meshFilters = new List<MeshFilter>();

    public void CombineMeshes()
    {
        meshFilters = GetComponentsInChildren<MeshFilter>().ToList();
        meshFilters.RemoveAt(0);

        CombineInstance[] combines = new CombineInstance[meshFilters.Count];

        for (int i = 0; i < meshFilters.Count; i++)
        {
            combines[i].mesh = meshFilters[i].sharedMesh;
            combines[i].transform = meshFilters[i].transform.localToWorldMatrix;
            meshFilters[i].gameObject.SetActive(false);
        }

        MeshFilter meshFilter = transform.GetComponent<MeshFilter>();
        meshFilter.mesh = new Mesh();
        
        meshFilter.mesh.CombineMeshes(combines);

        GetComponent<MeshCollider>().sharedMesh = meshFilter.mesh;
        transform.gameObject.SetActive(true);

    }
}
