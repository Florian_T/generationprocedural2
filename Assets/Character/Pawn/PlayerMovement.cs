﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody rb;
    Transform camera;
    public GameObject FreeCam;
    public Vector3 offset;

    [Range(0,10)]
    public float speed;

    [Range(0,10)]
    public float rotationSpeed;

    [Range(0, 10)]
    public float jumpForce;

    private bool grounded;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        camera = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        float H = Input.GetAxisRaw("Horizontal");
        float V = Input.GetAxisRaw("Vertical");

        Vector3 movement = (camera.forward * V + camera.right * H) * Time.deltaTime * speed;
        movement = new Vector3(movement.x, 0, movement.z);
        rb.MovePosition(rb.position + movement);

        if(Input.GetKey(KeyCode.RightShift) && grounded)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }

        grounded = Physics.Raycast(transform.position, Vector3.down, 0.51f);

        if(movement != Vector3.zero)
        {
            Quaternion direction = Quaternion.LookRotation(movement);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, direction, rotationSpeed);
        }

        if (Input.GetKey(KeyCode.F))
        {
            FreeCam.gameObject.SetActive(true);
            this.gameObject.SetActive(false);
            FreeCam.gameObject.transform.position = this.gameObject.transform.position + offset;
            FreeCam.gameObject.transform.rotation = this.gameObject.transform.rotation;
        }
    }
}
