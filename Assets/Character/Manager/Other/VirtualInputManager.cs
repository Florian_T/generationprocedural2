﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameManager
{
    public class VirtualInputManager : Singleton<VirtualInputManager>
    {
        public bool MoveHorizontal;
        public bool MoveVertical;
        public bool Jump;
    }
}