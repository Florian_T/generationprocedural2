﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameManager
{
    public class KeyBoardInput : MonoBehaviour
    {
        void Update()
        {
            if(Input.GetAxisRaw("Horizontal") > 0 || Input.GetAxisRaw("Horizontal") < 0)
            {
                VirtualInputManager.Instance.MoveHorizontal = true;
            }
            else
            {
                VirtualInputManager.Instance.MoveHorizontal = false;
            }

            if (Input.GetAxisRaw("Vertical") > 0 || Input.GetAxisRaw("Vertical") < 0)
            {
                VirtualInputManager.Instance.MoveVertical = true;
            }
            else
            {
                VirtualInputManager.Instance.MoveVertical = false;
            }

            if (Input.GetKey(KeyCode.RightShift))
            {
                VirtualInputManager.Instance.Jump = true;
            }
            else
            {
                VirtualInputManager.Instance.Jump = false;
            }
        }
    }
}
