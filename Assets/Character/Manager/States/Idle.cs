﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameManager
{
    [CreateAssetMenu(fileName = "NewState", menuName = "GameManager/AbilityData/Idle")]
    public class Idle : StateData
    {
        public override void OnEnter(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            animator.SetBool(TransitionParameter.Jump.ToString(), false);
        }
        public override void UpdateAbility(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            CharacterControl control = characterState.GetCharacterControl(animator);

            float h = Input.GetAxisRaw("Horizontal");
            float v = Input.GetAxisRaw("Vertical");

            Vector3 movement = (control.cam.forward * v + control.cam.right * h) * Time.deltaTime * control.speed;
            movement = new Vector3(movement.x, 0, movement.z);
            control.RIGID_BODY.MovePosition(control.RIGID_BODY.position + movement);

            if (h > 0.1f || h < -0.1f || v > 0.1f || v < -0.1f)
            {
                animator.SetBool(TransitionParameter.Move.ToString(), true);
            }

            if (control.Jump)
            {
                animator.SetBool(TransitionParameter.Jump.ToString(), true);
            }
        }

        public override void OnExit(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            
        }
    }
}
