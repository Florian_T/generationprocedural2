﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameManager
{
    [CreateAssetMenu(fileName = "New State", menuName = "GameManager/AbilityData/MoveForward")]
    public class MoveForward : StateData
    {
        public AnimationCurve SpeedGraph;
        public float speed;
        public float BlockDistance;

        public override void OnEnter(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            
        }
        public override void UpdateAbility(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            CharacterControl control = characterState.GetCharacterControl(animator);

            if (control.Jump)
            {
                animator.SetBool(TransitionParameter.Jump.ToString(), true);
            }

            float h = Input.GetAxisRaw("Horizontal");
            float v = Input.GetAxisRaw("Vertical");

            if (!CheckFront(control))
            {
                Vector3 movement = (control.cam.forward * v + control.cam.right * h) * Time.deltaTime * speed * SpeedGraph.Evaluate(stateInfo.normalizedTime);
                movement = new Vector3(movement.x, 0, movement.z);
                control.RIGID_BODY.MovePosition(control.RIGID_BODY.position + movement);

                if (movement != Vector3.zero)
                {
                    Quaternion direction = Quaternion.LookRotation(movement);
                    control.transform.rotation = Quaternion.RotateTowards(control.transform.rotation, direction, control.rotationSpeed);
                }

                if (movement != Vector3.zero)
                {
                    Quaternion direction = Quaternion.LookRotation(movement);
                    control.transform.rotation = Quaternion.RotateTowards(control.transform.rotation, direction, control.rotationSpeed);
                }
            }

            if (h < 0.1f && h > -0.1f && v < 0.1f && v > -0.1f)
            {
                animator.SetBool(TransitionParameter.Move.ToString(), false);
            }
        }

        public override void OnExit(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            
        }

        bool CheckFront(CharacterControl control)
        {
            foreach (GameObject o in control.FrontSpheres)
            {
                Debug.DrawRay(o.transform.position, control.transform.forward * 0.3f, Color.yellow);
                RaycastHit hit;
                if (Physics.Raycast(o.transform.position, control.transform.forward, out hit, BlockDistance))
                {
                    return true;
                }
            }
            return false;
        }
    }
}