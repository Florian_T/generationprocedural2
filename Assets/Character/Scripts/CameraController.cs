﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Transform lookAt; //What to look at 
    private Transform camTransform; // Main Camera 
    public bool FPSMode = false;
    [Range(0, 10)]
    public float TranslateSpeed;
    //Distance from player as a slider 
    //Max range being 30
    //Set Default to 10f
    //Attribute does not CLAMP value so it can go beyong 0 and 30
    [Range(0, 30)]
    public float distanceFromPlayer = 10.0f;
    //X position of the camera
    public float cameraX = 0.0f;
    //Y position of the camera
    public float cameraY = 30.0f;
    //Minimum Y camera position value
    public float minY = 1;
    public float minX = 0;
    //Maximum Y position camera value
    public float maxY = 80;
    public float maxX = 360;


    //Camera sensitivity multiplier
    //Used so large camera value's aren't needed
    private float sensitivty = 100;
    //Speed multipliers for camera X and Y movement
    //Range slider from 0 to 10
    [Range(0, 10)]
    public float xSpeed = 5f;
    [Range(0, 10)]
    public float ySpeed = 5;


    // Start is called before the first frame update
    void Start()
    {
        //Set the lookAt object to the player
        lookAt = GameObject.FindGameObjectWithTag("Player").transform;
        //Find the main camera and get the transform
        camTransform = Camera.main.transform;

        //Set the mouse to invisible
        Cursor.visible = false;
        //Lock the cursor in place so it doesnt move out of screen
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        //Clamps the speed value between 1 and 10
        // not necessary but  a good to have for possible future features
        ySpeed = Mathf.Clamp(ySpeed, 1, 10);
        xSpeed = Mathf.Clamp(xSpeed, 1, 10);

        //Grabs the mouse input and multiplies it by the ySpeed * sensitivity value * Time
        //Puts it into the camera Y position variable
        cameraY += -Input.GetAxis("Mouse Y") * ySpeed * sensitivty * Time.deltaTime;

        cameraX += Input.GetAxis("Mouse X") * xSpeed * sensitivty * Time.deltaTime;
        //Checks if pressing Q
        //if (Input.GetKey(KeyCode.Q))
        //{
            //subtract the cameraX value
            //cameraX -= xSpeed * sensitivty * Time.deltaTime;
        //}
        //If pressing E
        //if (Input.GetKey(KeyCode.E))
        //{
            //add to the cameraX value
            //cameraX += xSpeed * sensitivty * Time.deltaTime;
        //}
        //clamps the cameraY value so it doesn't go beyond a certain point
        cameraY = Mathf.Clamp(cameraY, minY, maxY);

        if (Input.GetKeyDown(KeyCode.P))
        {
            FPSMode = true;
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            FPSMode = false;
        }

        if (FPSMode == true)
        {
            distanceFromPlayer -= 1 * TranslateSpeed * Time.deltaTime;
        }

        if (FPSMode == false)
        {
            distanceFromPlayer += 1 * TranslateSpeed * Time.deltaTime;
        }

        distanceFromPlayer = Mathf.Clamp(distanceFromPlayer, 0.1f, 8);
    }

    private void LateUpdate()
    {
        //Puts distance into a Vector3
        Vector3 dir = new Vector3(0, 0, -distanceFromPlayer);
        //Puts X and Y distance values in for the camera
        Quaternion rotation = Quaternion.Euler(cameraY, cameraX, 0);
        // From lookAt object position it adds to get the position camera needs to be at
        camTransform.position = lookAt.position + rotation * dir;
        //Makes camera face the look at object
        camTransform.LookAt(lookAt);
    }
}